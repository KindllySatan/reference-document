# 定制 Peaks.js v2.1.0

本文档描述了如何在 Peaks.js 中自定义波形渲染和媒体播放的各个方面。

# 目录

- [简介](#1、简介)
- [记录点和记录段](#2、记录点和记录段)
  - [createPointMarker()](#2.1、`createPointMarker(options)`)
  - [createSegmentMarker()](#2.2、`createSegmentMarker(options)`)
  - [Marker 相关 API](#2.3、Marker 相关 API)
    - [marker.constructor()](#2.3.1、`marker.constructor(options)`)
    - [marker.init()](#2.3.2、`marker.init(group)`)
    - [marker.fitToView()](#2.3.3、`marker.fitToView()`)
    - [marker.timeUpdated()](#2.3.4、`marker.timeUpdated(time)`)
    - [marker.destroy()](#2.3.5、`marker.destroy()`)
  - [Layer 相关 API](#2.4、Layer 相关 API)
    - [layer.getHeight()](#2.4.1、`layer.getHeight()`)
    - [layer.draw()](#2.4.2、`layer.draw()`)
- [记号段标签](#3、记号段标签)
  - [createSegmentLabel()](#3.1、`createSegmentLabel(options)`)
- [媒体播放](#4、媒体播放)
  - [播放器界面](#4.1、播放器界面)
    - [player.init()](#4.1.1、player.init(eventEmitter))
    - [player.destroy()](#4.1.2、player.destroy())
    - [player.setSource()](#4.1.3、player.setSource(options))
    - [player.play()](#4.1.4、player.play())
    - [player.pause()](#4.1.5、player.pause())
    - [player.seek()](#4.1.6、player.seek(time))
    - [player.isPlaying()](#4.1.7、player.isPlaying())
    - [player.isSeeking()](#4.1.8、player.isSeeking())
    - [player.getCurrentTime()](#4.1.9、player.getCurrentTime())
    - [player.getDuration()](#4.1.10、player.getDuration())
  - [播放器事件](#4.2、播放器事件)
    - [player.canplay](#4.2.1、player.canplay 事件)
    - [player.error](#4.2.2、player.error 事件)
    - [player.playing](#4.2.3、player.playing 事件)
    - [player.pause](#4.2.4、player.pause 事件)
    - [player.seeked](#4.2.5、player.seeked 事件)
    - [player.timeupdate](#4.2.6、player.timeupdate 事件)
- [时间标签](#5、时间标签)

# 1、简介

<span style="color: orange;">**注意：**本文档中描述的 API 还不稳定，因此可能随时更改。</span>

Peaks.js 使用了 [Konva.js](https://konvajs.org/) 图形库，因此我们建议您熟悉 Konva。你可能会发现下面的教程很有用：

* [Konva 多边形教程](https://konvajs.github.io/docs/shapes/Line_-_Polygon.html)
* [Konva 文本教程](https://konvajs.github.io/docs/shapes/Text.html)
* [Konva 标签教程](https://konvajs.github.io/docs/shapes/Label.html)



# 2、记录点和记录段

Peaks.js 允许你自定义点和段标记的外观。这是通过在调用 `Peaks.init()` 时传递的选项中提供 `createPointMarker`，`createSegmentMarker` 函数两者或其一来实现的，例如：

```javascript
function createPointMarker(options) {
    // (见下文)
}

function createSegmentMarker(options) {
    // (见下文)
}

const options = {
    // 根据需要添加其他选项
    createPointMarker: createPointMarker,
    createSegmentMarker: createSegmentMarker
};

Peaks.init(options, function(err, peaks) {
    // 使用这里的 Peaks.js 实例
});
```

这里有一个完整的 [示例演示](https://github.com/bbc/peaks.js/tree/master/demo/custom-markers)，展示了如何使用这些函数绘制自定义点和段标记句柄。

值得注意的是，自定义标记功能不适用于 Peaks.js UMD 包引入。你必须将 Peaks.js 构建到你自己的捆绑包中，并将 Konva 作为对等依赖，使用模块捆绑器，如 [Webpack](https://webpack.js.org/)、[Rollup](https://rollupjs.org/)、[Parcel](https://parceljs.org/) 等。

## 2.1、`createPointMarker(options)`

`createPointMarker` 函数的作用是：返回一个渲染记号点标记句柄的对象。当被调用时，这个函数将接收到一个包含以下选项的对象：

| 名称         | 类型          | 描述                                                         |
| ------------ | ------------- | ------------------------------------------------------------ |
| `point`      | `Point`       | 与此标记句柄关联的 `Point` 对象，其提供了对 `time`， `color` 和 `labelText` 属性等的访问 |
| `view`       | `string`      | 创建标记句柄所在视图的标记，其值为 `zoomview` 或 `overview`  |
| `layer`      | `PointsLayer` | 渲染层，详见 [Layer 相关 API](#2.4、Layer 相关 API)          |
| `draggable`  | `boolean`     | 如果 `true`，则表示标记是可拖动的                            |
| `color`      | `string`      | 记号点标记句柄的颜色，其值由 `Peaks.init()` 中的 `pointMarkerColor` 选项设置 |
| `fontFamily` | `string`      | 标记句柄文本的字体族，其值由 `Peaks.init()` 中的 `fontFamily` 选项设置，默认值为 `sans-serif'` |
| `fontSize`   | `number`      | 标记句柄文本的字体大小，单位为 `px`，其值由 `Peaks.init()` 中的 `fontSize` 选项设置，默认值为 `10` |
| `fontShape`  | `string`      | 标记句柄文本的字体形状，其值由 `Peaks.init()` 中的 `fontShape` 选项设置，默认值为 `normal` |

该函数应该返回一个对象的实例，如下面的 `CustomPointMarker` 类所示，您可以使用 `view` 选项在缩放视图和概览波形视图中赋予标记不同的外观或行为。

```javascript
class CustomPointMarker {
    constructor(options) {
        // (必填项，见下文)
    }

    init(group) {
        // (必填项，见下文)
    }

    fitToView() {
        // (必填项，见下文)
    }

    timeUpdated() {
        // (必填项，见下文)
    }

    destroy() {
        // (必填项，见下文)
    }
};

function createPointMarker(options) {
    return new CustomPointMarker(options);
}
```

您的自定义点标记句柄对象必须实现 `init` 和 `fitToView` 方法。它还可以选择性地实现 `timeUpdated` 和 `destroy`。详细信息请参阅 [Marker 相关 API](#2.3、Marker 相关 API) 部分。

## 2.2、`createSegmentMarker(options)`

`createSegmentMarker` 函数的作用是：返回一个渲染记号段标记句柄的对象。当被调用时，这个函数接收到一个包含以下选项的对象：

| 名称             | 类型            | 描述                                                         |
| ---------------- | --------------- | ------------------------------------------------------------ |
| `segment`        | `Segment`       | 与这个标记句柄关联的 `Segment` 对象。这提供了对 `startTime` ，`endTime`，`color`和 `labelText` 等属性的访问 |
| `view`           | `string`        | 创建标记句柄所在视图的标记，其值为 `zoomview` 或 `overview`  |
| `layer`          | `SegmentsLayer` | 渲染层，详见 [Layer 相关 API](#2.4、Layer 相关 API)          |
| `draggable`      | `boolean`       | 表示标记是可拖动的，对于记号段标记句柄，这个值总是`true`     |
| `color`          | `string`        | 标记句柄的颜色，其值由`Peaks.init()` 的 `segmentOptions.startMarkerColor` 或 `segmentOptions.endMarkerColor` 设置 |
| `fontFamily`     | `string`        | 标记句柄文本的字体族，其值由 `Peaks.init()` 中的 `fontFamily` 选项设置，默认值为 `sans-serif'` |
| `fontSize`       | `number`        | 标记句柄文本的字体大小，单位为 `px`，其值由 `Peaks.init()` 中的 `fontSize` 选项设置，默认值为 `10` |
| `fontShape`      | `string`        | 标记句柄文本的字体形状，其值由 `Peaks.init()` 中的 `fontShape` 选项设置，默认值为 `normal` |
| `startMarker`    | `boolean`       | 如果其值为 `true`，标记表示段的开始时间，若为 `false`则标记表示段的结束时间 |
| `segmentOptions` | `object`        | 具有当前视图段显示选项的对象，详见 [README.zh-CN.md](README.zh-CN.md) 的 `segmentOptions` 相关内容 |

该函数应该返回一个对象的实例，如下面的 `CustomSegmentMarker` 类所示，您可以使用 `view` 选项在缩放视图和概览波形视图中赋予标记不同的外观或行为，如果不想显示段标记句柄，也可以从这个函数返回 `null` 

```javascript
class CustomSegmentMarker {
    constructor(options) {
        // (必填项，见下文)
    }

    init(group) {
        // (必填项，见下文)
    }

    fitToView() {
        // (必填项，见下文)
    }

    timeUpdated() {
        // (必填项，见下文)
    }

    destroy() {
        // (必填项，见下文)
    }
};

function createSegmentMarker(options) {
    return new CustomSegmentMarker(options);
}
```

您的自定义段标记句柄对象必须实现 `init` 和 `fitToView` 方法。它还可以选择性地实现 `timeUpdated` 和 `destroy`。详细信息请参阅 [Marker 相关 API](#2.3、Marker 相关 API) 部分。

## 2.3、Marker 相关 API

标记对象的构造分为两个阶段，首先你的代码使用 `new` 来创建标记对象，然后将提供的 `options` 传递给构造函数，Peaks.js 将调用 `init()` 方法来完成初始化。

### 2.3.1、`marker.constructor(options)`

构造函数通常只存储 `options` 以供以后使用。

```javascript
constructor(options) {
  this._options = options;
}
```

### 2.3.2、`marker.init(group)`

`init` 方法应该创建渲染标记句柄所需的 Konva 对象，并将它们添加到所提供的 `group` 对象中。

| 名称Name  | 类型                                                      | 描述                                                         |
| --------- | --------------------------------------------------------- | ------------------------------------------------------------ |
| `group`   | [`Konva.Group`](https://konvajs.org/api/Konva.Group.html) | 标记的 Konva 对象的容器                                      |
| `options` | `object`                                                  | 同样的选项传递给 [`createPointMarker`](#2.1、`createPointMarker(options)`) 或[`createSegmentMarker`](#2.2、`createSegmentMarker(options)`) |

下面的示例将点标记句柄创建为具有矩形句柄的垂直线，`x` 和 `y`坐标 `(0, 0)` 表示标记的中心和波形视图的顶部。

```javascript
class CustomPointMarker {
  constructor(options) {
    this._options = options;
  }

  init(group) {
    const layer = this._options.layer;
    const height = layer.getHeight();

    this._handle = new Konva.Rect({
      x:      -20,
      y:      0,
      width:  40,
      height: 20,
      fill:   this._options.color
    });

    this._line = new Konva.Line({
      points:      [0.5, 0, 0.5, height], // x1, y1, x2, y2
      stroke:      options.color,
      strokeWidth: 1
    });

    group.add(this._handle);
    group.add(this._line);
  }
}
```

如果需要，`init` 方法还可以添加您自己的自定义事件处理程序，例如：`mouseenter`和 `mouseleave`。

我们可以从上面的 `init` 方法末尾添加以下代码。当用户将鼠标悬停在标记句柄上时，此代码将更改标记句柄的颜色。

```javascript
const layer = this._options.layer;

this._handle.on('mouseenter', () => {
  const highlightColor = '#ff0000';
  this._handle.fill(highlightColor);
  this._line.stroke(highlightColor);
  layer.draw();
});

this._handle.on('mouseleave', () => {
  const defaultColor = this._options.color;
  this._handle.fill(defaultColor);
  this._line.stroke(defaultColor);
  layer.draw();
});
```

### 2.3.3、`marker.fitToView()`

`fitToView` 方法将在波形视图被调整大小后被调用。此方法应使用可用空间调整标记的大小。这通常是在视图的高度发生变化时执行的。

```javascript
fitToView() {
  const layer = this._options.layer;
  const height = layer.getHeight();

  this._line.points([0.5, 0, 0.5, height]);
}
```

### 2.3.4、`marker.timeUpdated(time)`

当标记的时间位置发生变化时，将调用 `timeUpdated` 方法。这是标记的 `time` 属性（用于记号点标记），或者 `startTime` 或 `endTime`（用于记号段标记）。

| 名称   | 类型     | 描述                   |
| ------ | -------- | ---------------------- |
| `time` | `number` | 标记时间位置，单位为秒 |

```javascript
timeUpdated(time) {
  console.log('Marker time', time);
}
```

### 2.3.5、`marker.destroy()`

`destroy` 方法在标记从视图中移除时被调用。在 `init()` 中添加到 `group` 中的任何 Konva 对象都将被自动销毁，因此您只需要在需要额外清理时添加 `destroy` 方法。

```javascript
destroy() {
  console.log('Marker destroyed');
}
```

## 2.4、Layer 相关 API

`PointsLayer` 和 `SegmentsLayer` 对象允许你获得关于 canvas 画布的信息，并对标记 Konva 对象进行渲染更改。

<span style="color: orange;">注意：`PointsLayer` 和 `SegmentsLayer` 并不是 Konva 层的对象。</span>

### 2.4.1、`layer.getHeight()`

返回图层的高度，单位为像素。

### 2.4.2、`layer.draw()`

如果你改变任何形状属性，Konva 通常会自动重绘，但如果需要，你可以主动调用这个函数来强制重绘 canvas 画布。

# 3、记号段标签

默认情况下，当用户将鼠标悬停在记号段上时，Peaks.js会显示记号段标签。该标签是通过调用`Peaks.init()`时传入的`createSegmentLabel`函数创建的 Konva 对象。

## 3.1、`createSegmentLabel(options)`

`createSegmentLabel`函数返回当用户将鼠标悬停在记号段上时显示的 Konva 对象，可以用来显示某个记号段的相关信息，比如它的`labelText`。

如果你不想显示记号段标签，你也可以从这个函数返回 `null` 来清除该记号段的所属标签。

| 名称      | 类型            | 描述                                                         |
| --------- | --------------- | ------------------------------------------------------------ |
| `segment` | `Segment`       | 与这个标签相关联的 `Segment` 对象。这提供了对 `startTime`， `endTime`，`color` 和 `labelText` 等属性的访问 |
| `view`    | `string`        | 创建标签所在视图的名称，其值为 `zoomview` 或 `overview`。    |
| `layer`   | `SegmentsLayer` | 渲染层，详见 [Layer 相关 API](#2.4、Layer 相关 API)          |

```javascript
function createSegmentLabel(options) {
    if (options.view === 'overview') {
        return null;
    }

    return new Konva.Text({
        text:       options.segment.labelText,
        fontSize:   14,
        fontFamily: 'Calibri',
        fill:       'black'
    });
}

const options = {
    // 根据需要添加其他选项
    createSegmentLabel: createSegmentLabel,
};

Peaks.init(options, function(err, instance) {
    // 使用这里的 Peaks.js 实例
});
```

# 4、媒体播放

Peaks.js 默认的媒体播放器基于 [HTMLMediaElement](https://html.spec.whatwg.org/multipage/media.html#media-elements)，但 Peaks.js 允许你与外部媒体播放器库进行交互，这对于基于 Web 音频的媒体播放器很有用，例如 [Tone.js](https://tonejs.github.io/) 或 [Howler.js](https://howlerjs.com/)。

基于 [HTMLMediaElement](https://html.spec.whatwg.org/multipage/media.html#media-elements) 的播放器应该按现状工作，而不需要你自定义 Peaks.js，而外部媒体播放器则可以通过实现下面描述的播放器界面来使用。你可以在这里找到一个完整的 [例子](https://github.com/bbc/peaks.js/blob/master/demo/external-player.html)，它展示了如何使用 [Tone.js](https://tonejs.github.io/) 实现这样一个播放器。

## 4.1、播放器界面

`player` 配置选项允许你传递一个将被调用的对象，你可以直接通过 Peaks.js [Player 相关 API](README.zh-CN.md#5.2、Player  相关 API) 进行配置，或者通过与波形视图交互（例如通过鼠标点击或键盘搜索）间接地进行。如果您正在使用 `player`，则不需要传递 `mediaElement` 配置选项。

`player` 界面的结构如下:

```javascript
const player = {
  init:           function(eventEmitter) { ... },
  destroy:        function() { ... },
  play:           function() { ... },
  pause:          function() { ... },
  seek:           function(time) { ... },
  isPlaying:      function() { ..., return boolean; },
  isSeeking:      function() { ..., return boolean; },
  getCurrentTime: function() { ..., return number; },
  getDuration:    function() { ..., return number; },
};

const options = {
  // 根据需要添加其他选项
  player: player
};

Peaks.init(options, function(err, instance) {
  // 使用这里的Peaks.js实例
});
```

### 4.1.1、player.init(eventEmitter)

初始化外部媒体播放器。这个方法在 Peaks.js 初始化时被调用，播放器实现应该存储 `eventEmitter` 以供以后使用。详细可见 [播放器事件](#4.2、播放器事件) 部分，了解您的自定义播放器应该如何使用 `eventEmitter` 与 `Peaks` 实例通信的更多细节。

为了允许可能涉及异步操作的初始化，播放器的实现应该返回一个`Promise` 以保证当外部媒体播放器已初始化。

```javascript
init(eventEmitter) {
  this.eventEmitter = eventEmitter;
  this.state = 'paused';
  this.interval = null;

  // 初始化外部播放器
  this.externalPlayer = new MediaPlayer();

  return Promise.resolve();
}
```

### 4.1.2、player.destroy()

释放播放器使用的资源。

```javascript
destroy() {
  if (this.interval !== null) {
    clearTimeout(this.interval);
    this.interval = null;
  }

  // Release the external player
  this.externalPlayer.destroy();
  this.externalPlayer = null;
}
```

### 4.1.3、player.setSource(options)

更改与 Peaks 实例关联的音频或视频媒体源。`options` 与传递给 [`peaks.setSource()`](README.zh-CN.md#5.1.2、`instance.setSource(options, callback)`) 的配置项等价。

该函数应该返回一个 Promise，以保证外部媒体播放器在该函数被调用前已完成更新。

```javascript
setSource(options) {
  // 使用给定的选项更新外部播放器

  return Promise.resolve();
}
```

### 4.1.4、player.play()

从当前播放位置开始播放。该函数可能会在播放实际开始时返回一个 Promise，播放开始时应当触发 [`player.playing`](#4.2.3、`player.playing` 事件)事件。

```javascript
play() {
  return this.externalPlayer.play().then(() => {
    this.state = 'playing';
    this.eventEmitter.emit('player.playing', this.getCurrentTime());
  });
}
```

### 4.1.5、player.pause()

暂停媒体播放。当播放暂停时，应该触发一个[`player.pause`](#4.2.4、`player.pause` 事件)事件。

```javascript
pause() {
  this.externalPlayer.pause().then(() => {
    this.state = 'paused';
    this.eventEmitter.emit('player.pause', this.getCurrentTime());
  });
}
```

### 4.1.6、player.seek(time)

请求给定时间段资源，单位秒。

```javascript
seek(time) {
  this.previousState = this.state; // 'paused' or 'playing'
  this.state = 'seeking';

  this.externalPlayer.seek(time).then(() => {
    this.state = this.previousState;
    this.eventEmitter.emit('player.seeked', this.getCurrentTime());
    this.eventEmitter.emit('player.timeupdate', this.getCurrentTime());
  });
}
```

### 4.1.7、player.isPlaying()

如果播放器当前正在播放，则返回`true`，否则返回`False`。

```javascript
pause() {
  return this.state === 'playing';
}
```

### 4.1.8、player.isSeeking()

如果播放器当前正在请求资源，则返回`true`，否则返回`False`。

```javascript
pause() {
  return this.state === 'seeking';
}
```

### 4.1.9、player.getCurrentTime()

返回当前媒体播放位置，单位秒。

```javascript
getCurrentTime() {
  return this.externalPlayer.currentTime;
}
```

### 4.1.10、player.getDuration()

返回媒体总长时间，单位秒。

```javascript
getDuration() {
  return this.externalPlayer.duration;
}
```

## 4.2、播放器事件

定制播放器和 Peaks.js 之间的通信是通过事件完成的，Peaks.js 使用这些事件来更新它的内部状态，比如播放指针在屏幕上的位置。你的自定义播放器应该发出事件来通知 Peaks.js 播放器内的状态变化。

这些播放器事件是基于[`HTMLMediaElement` 事件](https://html.spec.whatwg.org/multipage/media.html#mediaevents)，为了使 Peaks.js 能够正确地更新其内部状态并直观地反映播放器状态的变化，播放器事件应只在相应的播放器动作触发后才会被触发。

### 4.2.1、player.canplay 事件

通知 Peaks.js 媒体已经准备好播放。

```javascript
this.eventEmitter.emit('player.canplay');
```

### 4.2.2、player.error 事件

通知 Peaks.js 发生播放器内部错误，例如获取媒体数据失败。事件数据应该是一个 `Error` 对象。

```javascript
this.eventEmitter.emit('player.error', new Error("Failed to start playback"));
```

### 4.2.3、player.playing 事件

通知 Peaks.js 媒体播放已经开始。事件数据应该是当前播放位置，以秒为单位。

```javascript
this.eventEmitter.emit('player.playing', this.getCurrentTime());
```

### 4.2.4、player.pause 事件

通知 Peaks.js 媒体播放已经停止或暂停。事件数据应该是当前播放位置，以秒为单位。

```javascript
this.eventEmitter.emit('player.pause', this.getCurrentTime());
```

### 4.2.5、player.seeked 事件

通知 Peaks.js 一个资源请求操作已经完成。事件数据应该是当前播放位置，以秒为单位。

```javascript
this.eventEmitter.emit('player.seeked', this.getCurrentTime());
```

### 4.2.6、player.timeupdate 事件

通知 Peaks.js 当前播放位置已经改变。为了模拟 `HTMLMediaElement` 行为，在媒体播放期间，这个事件应该大约每250毫秒触发一次。它也应在成功的资源请求操作之后被抛出。事件数据应该是当前播放位置，以秒为单位。

```javascript
this.eventEmitter.emit('player.timeupdate', this.getCurrentTime());
```

# 5、时间标签

Peaks.js 允许你自定义时间戳标签显示在时间轴上或播放指针旁边。这是通过在调用 `Peaks.init()` 时传递的选项中提供 `formatPlayheadTime` 和 `formatAxisTime` 函数来实现的。

这些函数接受一个时间参数（`number` 类型），并且应该返回一个 `string` 类型返回值，例如：

```javascript
function formatPlayheadTime(time) {
  // 返回格式化为小数点后 2 位的时间
  return time.toFixed(2);
}

function formatAxisTime(time) {
  // 返回以秒数为单位的时间
  return String(Math.floor(time));
}

const options = {
  // 根据需要添加其他选项
  formatPlayheadTime: formatPlayheadTime,
  formatAxisTime: formatAxisTime
};

Peaks.init(options, function(err, peaks) {
  // 使用这里的 Peaks.js 实例
});
```

<span style="color: orange;">注意：如果你主动传递一个 `formatPlayheadTime` 函数，`timeLabelPrecision` 选项和 `view.setTimeLabelPrecision()` 函数将会被忽略。</span>
