[![Build Status](https://github.com/bbc/peaks.js/workflows/Node.js%20CI/badge.svg?branch=master)](https://github.com/bbc/peaks.js/actions)

<p align="center">
    <b>Peaks.js v2.1.0 自用中文翻译文档</b>
</p>

<p align="center">
  <strong>用于在浏览器中显示音频波形并与其交互的客户端 JavaScript 组件</strong>
</p>

Peak.js 是由（[BBC R&D](https://www.bbc.co.uk/rd)）开发的，允许用户使用为波形数据提供服务的后端 API，在浏览器中对音频内容进行准确的剪辑。

`Peaks.js` 使用 HTML 画布元素以不同的缩放级别显示波形，并具有允许您自定义波形视图的配置选项，`Peaks.js` 允许用户与波形视图交互，包括缩放和滚动，以及创建点或段标记以表示要剪切或用于参考的内容，例如，区分音乐和语音或识别不同的音乐曲目。

# Features

* 可缩放和可滚动的波形视图
* 定宽波形视图
* 鼠标、触摸、滚轮和键盘交互
* 使用 Web Audio API 进行客户端波形计算
* 服务器端波形计算，提高效率
* 单声道、立体声或多通道波形视图
* 创建点或段标记注释
* 可定制的波形视图

您可以阅读有关该项目的更多信息并查看 [演示 Demo](https://waveform.prototyping.bbc.co.uk/)（译者注：该演示 Demo 需要科学上网才可以正常访问）

# 目录

- [快速开始](#1、快速开始)
  - [安装 Peaks.js](#1.1、安装 Peaks.js)
    - [通过 script  标签引入](#1.1.1、通过 script 标签引入)
    - [使用 npm 安装](#1.1.2、使用 npm 安装)
  - [将 Peaks.js 添加到你的网页中](#1.2、将 Peaks.js 添加到你的网页中)
  - [初始化 Peaks.js](#1.3、初始化 Peaks.js)
    - [通过 script  标签引入](#1.3.1、通过 script 标签引入)
    - [将其作为 ES2015 module 引入](#1.3.2、将其作为 ES2015 module 引入)
  - [后续步骤](#1.4、后续步骤)
- [演示 Demo](#2、演示 Demo)
- [生成波形数据](#3、生成波形数据)
  - [服务器预先计算波形数据](#3.1、服务器预先计算波形数据)
  - [使用 Web Audio API 计算波形数据](#3.2、使用 Web Audio API 计算波形数据)
- [配置](#4、配置)
  - [标记自定义](#4.1、标记自定义)
  - [播放器定制](#4.2、播放器定制)
  - [时间标签定制](#4.3、时间标签定制)
- [API](#5、API)
  - [生命周期相关 API](#5.1、生命周期相关 API)
    - [Peaks.init()](#5.1.1、`Peaks.init(options, callback)`)
    - [instance.setSource()](#5.1.2、`instance.setSource(options, callback)`)
    - [instance.destroy()](#5.1.3、`instance.destroy()`)
  - [Player 相关 API](#5.2、Player  相关 API)
    - [instance.player.play()](#5.2.1、`instance.player.play()`)
    - [instance.player.pause()](#5.2.2、`instance.player.pause()`)
    - [instance.player.getCurrentTime()](#5.2.3、`instance.player.getCurrentTime()`)
    - [instance.player.getDuration()](#5.2.4、`instance.player.getDuration()`)
    - [instance.player.seek()](#5.2.5、`instance.player.seek(time)`)
    - [instance.player.playSegment()](#5.2.6、`instance.player.playSegment(segment[, loop])`)
  - [Views 相关 API](#5.3、Views 相关 API)
    - [instance.views.getView()](#5.3.1、`instance.views.getView(name)`)
    - [instance.views.createZoomview()](#5.3.2、`instance.views.createZoomview(container)`)
    - [instance.views.createOverview()](#5.3.3、`instance.views.createOverview(container)`)
    - [instance.views.destroyZoomview()](#5.3.4、`instance.views.destroyZoomview()`)
    - [instance.views.destroyOverview()](#5.3.5、`instance.views.destroyOverview()`)
  - [View 相关 API](#5.4、View 相关 API)
    - [view.setAmplitudeScale()](#5.4.1、`view.setAmplitudeScale(scale)`)
    - [view.setWaveformColor()](#5.4.2、`view.setWaveformColor(color)`)
    - [view.setPlayedWaveformColor()](#5.4.3、`view.setPlayedWaveformColor(color)`)
    - [view.showPlayheadTime()](#5.4.4、`view.showPlayheadTime(show)`)
    - [view.setTimeLabelPrecision()](#5.4.5、`view.setTimeLabelPrecision(precision)`)
    - [view.showAxisLabels()](#5.4.6、`view.showAxisLabels(show)`)
    - [view.enableAutoScroll()](#5.4.7、`view.enableAutoScroll(enable)`)
    - [view.enableMarkerEditing()](#5.4.8、`view.enableMarkerEditing(enable)`)
    - [view.fitToContainer()](#5.4.9、`view.fitToContainer()`)
    - [view.setZoom()](#5.4.10、`view.setZoom(options)`)
    - [view.setStartTime()](#5.4.11、`view.setStartTime(time)`)
    - [view.scrollWaveform()](#5.4.12、`view.scrollWaveform(options)`)
    - [view.setWheelMode()](#5.4.13、`view.setWheelMode(mode[, options])`)
    - [view.enableSeek()](#5.4.14、`view.enableSeek(enable)`)
  - [Zoom 相关 API](#5.5、Zoom 相关 API)
    - [instance.zoom.zoomOut()](#5.5.1、`instance.zoom.zoomOut()`)
    - [instance.zoom.zoomIn()](#5.5.2、`instance.zoom.zoomIn()`)
    - [instance.zoom.setZoom()](#5.5.3、`instance.zoom.setZoom(index)`)
    - [instance.zoom.getZoom()](#5.5.4、`instance.zoom.getZoom()`)
  - [Segments 相关 API](#5.6、Segments  相关 API)
    - [instance.segments.add()](#5.6.1、`instance.segments.add({ startTime, endTime, editable, color, labelText, id[, ...] }) 或 instance.segments.add(segment[])`)
    - [instance.segments.getSegments()](#5.6.2、`instance.segments.getSegments()`)
    - [instance.segments.getSegment()](#5.6.3、`instance.segments.getSegment(id)`)
    - [instance.segments.removeByTime()](#5.6.4、`instance.segments.removeByTime(startTime[, endTime])`)
    - [instance.segments.removeById()](#5.6.5、`instance.segments.removeById(segmentId)`)
    - [instance.segments.removeAll()](#5.6.6、`instance.segments.removeAll()`)
  - [Segment 相关 API](#5.7、Segment 相关 API)
    - [segment.update()](#5.7.1、`segment.update({ startTime, endTime, labelText, color, editable[, ...] })`)
  - [Points 相关 API](#5.8、Points 相关 API)
    - [instance.points.add()](#5.8.1、`instance.points.add({ time, editable, color, labelText, id[, ...] }) 或 instance.points.add(point[])`)
    - [instance.points.getPoints()](#5.8.2、`instance.points.getPoints()`)
    - [instance.points.getPoint()](#5.8.3、`instance.points.getPoint(id)`)
    - [instance.points.removeByTime()](#5.8.4、`instance.points.removeByTime(time)`)
    - [instance.points.removeById()](#5.8.5、`instance.points.removeById(pointId)`)
    - [instance.points.removeAll()](#5.8.6、`instance.points.removeAll()`)
  - [Point 相关 API](#5.9、Point 相关 API)
    - [point.update()](#5.9.1、`point.update({ time, labelText, color, editable[, ...] })`)
- [事件](#6、事件)
  - [instance.on()](#6.1、`instance.on(event, callback)`)
  - [instance.once()](#6.2、`instance.once(event, callback)`)
  - [instance.off()](#6.3、`instance.off(event, callback)`)
  - [相关事件](#6.4、相关事件)
    - [生命周期相关事件](#6.4.1、生命周期相关事件)
    - [Player 相关事件](#6.4.2、Player 相关事件)
    - [Views 相关事件](#6.4.3、Views 相关事件)
    - [Waveforms 相关事件](#6.4.4、Waveforms 相关事件)
    - [Segments 相关事件](#6.4.5、Segments 相关事件)
    - [Points 相关事件](#6.4.6、Points 相关事件)
    - [提示事件](#6.4.7、提示事件)
- [构建 Peaks.js](#7、构建 Peaks.js)
  - [前置操作](#7.1、前置操作)
  - [构建](#7.2、构建)
  - [测试](#7.3、测试)
- [贡献](#贡献)
- [License 开源许可协议](#License 开源许可协议)
- [开发人员](#开发人员)
- [版权申明](#版权申明)

# 1、快速开始

## 1.1、安装 Peaks.js

你可以通过在你的网页的 `<script>` 标签中包含 UMD 包来开始使用 Peaks.js，或者使用 npm 或 yarn 来安装它，并将它包含在 [Webpack](https://webpack.js.org/)、[Rollup](https://rollupjs.org/)、[Parcel](https://parceljs.org/) 等模块包中。

### 1.1.1、通过 script 标签引入

要将 Peaks.js UMD 包添加到你的网页，添加一个 `<script>` 标签：

```html
<script src="https://unpkg.com/peaks.js/dist/peaks.js"></script>
```

UMD 包可以在 [unpkg](https://unpkg.com/peaks.js) 和 [cdnjs](https://cdnjs.com/libraries/peaks.js) 上获得。

### 1.1.2、使用 npm 安装

我们建议您使用 ES 模块绑定器，运行以下命令将 Peaks.js 包含在你的模块包中：

```bash
npm install --save peaks.js
npm install --save konva
npm install --save waveform-data
```

<span style="color: orange;">注意</span>：Peaks.js 使用 [Konva](https://konvajs.org/) 和 [waveform-data](https://github.com/bbc/waveform-data.js) 作为对等依赖项，所以在安装 Peaks.js 的同时你也必须安装这些模块。

## 1.2、将 Peaks.js 添加到你的网页中

为了在你的网页中使用 Peaks.js ，你需要添加数个 `<div>` 元素（译者注：也可以使用除 `<img>, <canvas>, <svg>` 的其他任意容器类元素，不过还是推荐使用 `<div>`）作为容器，Peaks.js 将使用这些容器元素来呈现波形视图，同时还需要一个 [media元素](https://developer.mozilla.org/en-US/docs/Web/API/HTMLMediaElement) 用于音频或视频内容的解析加载。

下面是一个 HTML 片段示例：

```html
<div id="zoomview-container"></div>
<div id="overview-container"></div>
<audio>
    <source src="sample.mp3" type="audio/mpeg">
    <source src="sample.ogg" type='audio/ogg codecs="vorbis"'>
</audio>
```

需要注意的是，作为容器的元素（例如示例中的 `<div>`）不应该包含任何内容，如上所示，因为它们的内容将被波形视图的 `canvas` 元素所取代。它们还应该设置所需的宽度和高度：

```css
#zoomview-container, #overview-container {
    width: 1000px;
    height: 100px;
}
```

## 1.3、初始化 Peaks.js

下一步是使用 [Peaks.init()](#5.1.1、`Peaks.init(options, callback)`) 和您自己的选择初始化 `Peaks` 实例。有关可用选项的详细信息，请参阅 [配置](#4、配置) 部分。

### 1.3.1、通过 script 标签引入

```html
<script src="https://unpkg.com/peaks.js/dist/peaks.js"></script>
<script>
    (function(Peaks) {
        const options = {
            zoomview: {
                container: document.getElementById('zoomview-container')
            },
            overview: {
                container: document.getElementById('overview-container')
            },
            mediaElement: document.querySelector('audio'),
            webAudio: {
                audioContext: new AudioContext()
            }
        };

        Peaks.init(options, function(err, peaks) {
            if (err) {
                console.error('初始化峰值实例失败: ' + err.message);
                return;
            }

            // 当波形显示并准备就绪时做一些事情
        });
    })(peaks);
</script>
```

### 1.3.2、将其作为 ES2015 module 引入

```javascript
import Peaks from 'peaks.js';

const options = {
    zoomview: {
        container: document.getElementById('zoomview-container')
    },
    overview: {
        container: document.getElementById('overview-container')
    },
    mediaElement: document.querySelector('audio'),
    webAudio: {
        audioContext: new AudioContext()
    }
};

Peaks.init(options, function(err, peaks) {
    if (err) {
        console.error('初始化峰值实例失败: ' + err.message);
        return;
    }

    // 当波形显示并准备就绪时做一些事情
});
```

## 1.4、后续步骤

我们建议你看一下 [演示 Demo](#2、演示 Demo)，它展示了如何使用 Peaks.js 提供的各种选项和 API。

阅读 [生成波形数据](#3、生成波形数据) 部分，了解如何使用预先计算或 Web Audio 生成的波形数据。

还可以参考 [配置](#4、配置) 部分了解所有 `Peaks.init()` 配置选项以及更高级的定制选项的详细信息，以及 [API](#5、API) 部分了解可用的 API 方法。

# 2、演示 Demo

[演示 demo 源码](https://github.com/bbc/peaks.js/tree/master/demo) 文件夹包含一些正在使用的 Peaks.js 的工作示例。要查看这些信息，输入以下命令：

```bash
git clone git@github.com:bbc/peaks.js.git
cd peaks.js
npm install
npm start
```

然后打开浏览器 http://localhost:8080 即可访问演示 Demo。除此之外还有一些额外的示例项目展示了如何在一些流行的 JavaScript 框架中使用 Peaks.js：

* [React](https://github.com/chrisn/peaksjs-react-example)
* [Angular](https://github.com/chrisn/peaksjs-angular-example)

# 3、生成波形数据

Peaks.js 通过处理音频数据生成波形数据来创建音频波形可视化，下面有两种不同的解决方案可以做到这一点:

* 从音频中预先计算波形数据，使用 [audiowaveform](https://github.com/bbc/audiowaveform)，并从您的 Web 服务器提供数据到 Peaks.js
* 在浏览器中使用 Web Audio API 计算波形数据

使用 Web Audio API 可以很好地处理短音频文件，但需要将整个音频文件下载到浏览器，并且占用大量CPU。对于较长的音频文件，预先计算波形数据是可取的，因为它节省了用户的带宽，并允许波形更快地呈现。

## 3.1、服务器预先计算波形数据

Peaks.js 可以使用 [audiowaveform](https://github.com/bbc/audiowaveform) 所生成的波形数据文件，这些文件通常以二进制 `.dat` 或 JSON 格式生成。考虑到文件大小，默认首选二进制格式。

当生成波形数据文件时，您应该使用 `-b 8` 选项（译者注：即 8 位波形数据），因为 Peaks.js 目前不支持16位波形数据文件，并且还需要最小化文件大小。

生成二进制波形数据文件指令：

```
audiowaveform -i sample.mp3 -o sample.dat -b 8
```

生成 JSON 格式的波形数据文件指令：

```
audiowaveform -i sample.mp3 -o sample.json -b 8
```

具体可参考 [audiowaveform 文档](https://github.com/bbc/audiowaveform) 获得可用命令行选项的完整细节，或运行以下指令查看帮助手册：

```bash
man audiowaveform
```

当你创建了一个波形数据文件之后，你需要在 Peaks.js 中通过传递 `dataUri` 配置项给 `Peaks.init()` 来使用它：

```javascript
import Peaks from 'peaks.js';

const options = {
    zoomview: {
        container: document.getElementById('zoomview-container')
    },
    overview: {
        container: document.getElementById('overview-container')
    },
    mediaElement: document.querySelector('audio'),
    dataUri: {
        arraybuffer: 'sample.dat' // or json: 'sample.json'
    }
};

Peaks.init(options, function(err, peaks) {
    // 在波形显示和准备就绪时做一些事情，或处理错误
});
```

## 3.2、使用 Web Audio API 计算波形数据

<span style="color: orange">译者注：使用该方法需要在 Web 应用根模板页面的安全策略指令中包含 `blob:`，否则在浏览器环境下会阻止该 API 的运行</span>，例如：

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- ... -->
        <meta http-equiv="Content-Security-Policy" content="default-src 'self' 'unsafe-inline' blob:;">
    </head>

    <body>
        ...
    </body>
</html>
```

Peaks.js 可以使用 [Web Audio API](https://www.w3.org/TR/webaudio/) 来生成波形，这意味着您不必通过服务器预先计算波形数据文件。

使用 Web Audio API 时，不再需要向 `peaks.init()` 传递 `dataUri` 配置项，取而代之的是需要传递一个包含 `AudioContext` 实例的 `webAudio` 对象。因此，您的浏览器必须[支持](https://caniuse.com/#feat=audio-api)  Web Audio API。

```js
import Peaks from 'peaks.js';

const audioContext = new AudioContext();

const options = {
    zoomview: {
        container: document.getElementById('zoomview-container')
    },
    overview: {
        container: document.getElementById('overview-container')
    },
    mediaElement: document.querySelector('audio'),
    webAudio: {
        audioContext: audioContext,
        scale: 128,
        multiChannel: false
    }
};

Peaks.init(options, function(err, peaks) {
    // 在波形显示和准备就绪时做一些事情，或处理错误
});
```

如果你有一个 `AudioBuffer` 包含解码音频样本，例如，从 [AudioContext.decodeAudioData](https://developer.mozilla.org/en-US/docs/Web/API/BaseAudioContext/decodeAudioData) 中获取的解码音频样本，那么就不需要传递 `AudioContext` 配置项：

```js
import Peaks from 'peaks.js';

const audioContext = new AudioContext();

// arrayBuffer 包含解码的音频(例如: MP3格式)
audioContext.decodeAudioData(arrayBuffer).then(function(audioBuffer) {
    const options = {
        zoomview: {
            container: document.getElementById('zoomview-container')
        },
        overview: {
            container: document.getElementById('overview-container')
        },
        mediaElement: document.querySelector('audio'),
        webAudio: {
            audioBuffer: audioBuffer
        }
    };

    Peaks.init(options, function(err, peaks) {
        // 在波形显示和准备就绪时做一些事情，或处理错误
    });
});
```

# 4、配置

Peaks.js 提供了许多配置选项，如下所示：

```javascript
var options = {
    /**
      * 缩放波形视图选项
      * */
    zoomview: {
        // 设定一个空的容器标签(例如 <div>)
        container: document.getElementById('zoomview-container'),

        // 设定缩放波形的颜色, 你也可以在这里使用 2 级颜色渐变, 详细可见 setWaveformColor()
        waveformColor: 'rgba(0, 225, 128, 1)',

        // 缩放波形的已播放区域的颜色, 你也可以在这里使用 2 级颜色渐变, 详细可见 setWaveformColor()
        playedWaveformColor: 'rgba(0, 225, 128, 1)',

        // 播放指针的颜色
        playheadColor: 'rgba(0, 0, 0, 1)',

        // 播放指针所属文本的颜色
        playheadTextColor: '#aaa',

        // 缩放视图中的触发的点击距离播放指针不超过多少像素时被解释为移动播放指针, 超出该距离则解释为拖动缩放视图显示时间轴
        playheadClickTolerance: 3,

        // 用于指定一个设置 播放指针所属当前所指时间文本 的函数, 该函数返回值应为一个 表示 播放指针所属当前所指时间文本 的字符串
        formatPlayheadTime: function,

        // 是否在播放指针旁边显示当前指针所指时间
        showPlayheadTime: false,

        // 设置 播放指针及点-段标记 所属当前所指时间文本的时间标签精度, 默认为 2, 每加1则表示显示秒位后多少精度
        timeLabelPrecision: 2,

        // 轴网格刻度线的颜色
        axisGridlineColor: '#ccc',

        // 轴网格刻度线所属文本的颜色
        axisLabelColor: '#aaa',

        // 用于指定一个设置 轴网格刻度线所属文本 的函数, 该函数返回值应为一个 表示 轴网格刻度线所属文本 的字符串
        formatAxisTime: function,

        // 显示或隐藏轴网格刻度线所属文本
        showAxisLabels: true,

        // 设置轴网格刻度线、播放指针、点-段标记 相关文本的 Font family
        fontFamily: 'sans-serif',

        // 设置轴网格刻度线、播放指针、点-段标记 相关文本的 字体大小
        fontSize: 11,

        // 设置轴网格刻度线、播放指针、点-段标记 相关文本的 字体样式(可选值: 'normal', 'bold', or 'italic')
        fontStyle: 'normal',

        // 设置鼠标滚轮模式 (可选值: 'none', 'scroll', or 'italic')
        wheelMode: 'none'
    },
    
    /**
      * 概览波形视图选项
      * */
    overview: {
        // 设定一个空的容器标签(例如 <div>)
        overview: document.getElementById('overview-container')

        // 概述波形的颜色, 你也可以在这里使用 2 级颜色渐变, 详细可见 setWaveformColor()
        waveformColor: 'rgba(0,0,0,0.2)',

        // 概览波形的已播放区域的颜色, 你也可以在这里使用 2 级颜色渐变, 详细可见 setWaveformColor()
        playedWaveformColor: 'rgba(0, 225, 128, 1)',

        // 概览波形视图矩形(即缩放波形显示视图时间轴在概略波形视图中的时间轴区域)的颜色, 你也可以在这里使用 2 级颜色渐变, 详细可见 setWaveformColor()
        highlightColor: 'grey',

        // 概览波形视图矩形的描边颜色
        highlightStrokeColor: 'transparent',

        // 概览波形视图矩形的不透明度
        highlightOpacity: 0.3,

        // 概览波形视图矩形的圆角半径
        highlightCornerRadius: 2,

        // 概览波形视图矩形的上下边距占用的默认像素数
        highlightOffset: 11,

        // 播放指针的颜色
        playheadColor: 'rgba(0, 0, 0, 1)',

        // 播放指针所属文本的颜色
        playheadTextColor: '#aaa',

        // 用于指定一个设置 播放指针所属当前所指时间文本 的函数, 该函数返回值应为一个 表示 播放指针所属当前所指时间文本 的字符串
        formatPlayheadTime: function,

        // 是否在播放指针旁边显示当前指针所指时间
        showPlayheadTime: false,

        // 设置 播放指针及点-段标记 所属当前所指时间文本的时间标签精度, 默认为 2, 每加1则表示显示秒位后多少精度
        timeLabelPrecision: 2,

        // 轴网格刻度线的颜色
        axisGridlineColor: '#ccc',

        // 轴网格刻度线所属文本的颜色
        axisLabelColor: '#aaa',

        // 用于指定一个设置 轴网格刻度线所属文本 的函数, 该函数返回值应为一个 表示 轴网格刻度线所属文本 的字符串
        formatAxisTime: function,

        // 显示或隐藏轴网格刻度线所属文本
        showAxisLabels: true,

        // 设置轴网格刻度线、播放指针、点-段标记 相关文本的 Font family
        fontFamily: 'sans-serif',

        // 设置轴网格刻度线、播放指针、点-段标记 相关文本的 字体大小
        fontSize: 11,

        // 设置轴网格刻度线、播放指针、点-段标记 相关文本的 字体样式(可选值: 'normal', 'bold', or 'italic')
        fontStyle: 'normal',
    },

    /**
      * 包含目标音轨的 HTML 媒体元素
      * */
    mediaElement: document.querySelector('audio'),

    /**
      * 预计算的波形数据配置
      * */
    dataUri: {
        // 二进制波形数据URL
        arraybuffer: '/data/sample.dat',

        // JSON格式波形数据URL
        json: '/data/sample.json',
    },

    /**
      * 音频波形数据相关配置
      * */
    waveformData: {
        // 包含二进制格式波形数据的 ArrayBuffer
        arraybuffer: null,

        // 包含 JSON 格式波形数据的对象
        json: null
    },

    /**
      * 凭证设置, 如果为真，Peaks.js将与所有网络请求一起发送凭证, 例如: 当获取波形数据时
      * */
    withCredentials: false,

	/**
      * Web 音频生成的波形数据配置选项
      * */
    webAudio: {
        // 一个 Web Audio AudioContext 实例，如果没有提供 dataUri，则使用它来呈现音频波形
        // 在不违背相关安全策略指令(blob:)的情况下, 它将会自动调用相关 Web Audio API 解析目标音频得到音频波形数据
        audioContext: new AudioContext(),

        // 提供一个包含解码音频样本的 AudioBuffer。在配置了该配置参数的情况下，不需要配置 audioContext 参数
        audioBuffer: null,

        // 多声道波形展示(true - 波形将显示所有可用的声道, false - 音频将显示为单个声道波形)
        multiChannel: false
    },

    /**
      * 设置波形视图中每像素的缩放级别数组, 数字越小，缩放比例越大
      * */
    zoomLevels: [512, 1024, 2048, 4096],

    /**
      * 是否保存不同缩放级别的波形缓存, 默认启用
      * 在设置为 false 时不缓存不同缩放级别的波形数据, 这会在缩放级别发生改变时消耗大量性能重新计算波形
      * */
    waveformCache: true

    /**
      * 键盘监听相关功能
      * */
    // 是否开启键盘控件监听
    keyboard: false,
    
    // 左右方向键轻推增量, 以秒为单位
    nudgeIncrement: 0.01,

    /**
      * 默认波形视图配置, 当然也可以在'zoomview'和'overview'配置项中(如上所述)下为每个波形视图独立设置
      * */
    // 波形颜色, 你也可以在这里使用 2 级颜色渐变, 详细可见 setWaveformColor()
    waveformColor: 'rgba(0, 225, 128, 1)',

    // 已播放区域波形的颜色, 你也可以在这里使用 2 级颜色渐变, 详细可见 setWaveformColor()
    playedWaveformColor: 'rgba(0, 225, 128, 1)',

    // 播放指针颜色, 你也可以在这里使用 2 级颜色渐变, 详细可见 setWaveformColor()
    playheadColor: 'rgba(0, 0, 0, 1)',

    // 播放指针所属文本颜色, 你也可以在这里使用 2 级颜色渐变, 详细可见 setWaveformColor()
    playheadTextColor: '#aaa',

    // 轴网格刻度线颜色, 你也可以在这里使用 2 级颜色渐变, 详细可见 setWaveformColor()
    axisGridlineColor: '#ccc',

    // 轴网格刻度线所属文本颜色, 你也可以在这里使用 2 级颜色渐变, 详细可见 setWaveformColor()
    axisLabelColor: '#aaa',

    // 设置轴网格刻度线、播放指针、点-段标记 相关文本的 Font family
    fontFamily: 'sans-serif',

    // 设置轴网格刻度线、播放指针、点-段标记 相关文本的 字体大小
    fontSize: 11,

    // 设置轴网格刻度线、播放指针、点-段标记 相关文本的 字体样式(可选值: 'normal', 'bold', or 'italic')
    fontStyle: 'normal',

    // 设置 播放指针及点-段标记 所属当前所指时间文本的时间标签精度, 默认为 2, 每加1则表示显示秒位后多少精度
    timeLabelPrecision: 2,

    // 在播放指针旁边显示当前时间(注: 仅限缩放视图)【译者注: 概略视图该功能需要在 overview 中配置 showPlayheadTime 】
    showPlayheadTime: false,

    /**
      * 点-段 配置选项
      * */
    // 记号点的颜色
    pointMarkerColor: '#FF0000',

    // 记号段开始标记时记号笔的配置颜色
    segmentStartMarkerColor: '#a0a0a0',

    // 记号段结束标记时记号笔的配置颜色
    segmentEndMarkerColor: '#a0a0a0',

    // 波形上分段的颜色
    segmentColor: 'rgba(255, 161, 39, 1)',

    // 是否开启记号段的随机颜色(覆盖主动设置的记号段颜色)
    randomizeSegmentColor: true,

    // 是否抛出记号事件, 如果为 true，则在Peaks实例上发出提示事件(详细参见 提示事件)
    emitCueEvents: false,

    /**
      * 自定义选项(详细参见 customizing.md)
      * */
    createSegmentMarker: null,
    createSegmentLabel: null,
    createPointMarker: null,
    player: null,

    /**
      * 点-段初始化
      * */
    // 记号段相关配置
    segments: [
        {
            // 记号段开始时间, 单位秒
            startTime: 120,
    
            // 记号段结束时间, 单位秒
            endTime: 140,
    
            // 记号段是否可编辑
            editable: true,
    
            // 记号段颜色
            color: "#ff0000",
    
            // 记号段标记文本
            labelText: "My label"
        },
    ],

    // 记号点相关配置
    points: [
        {
            // 记号点时间, 单位秒
            time: 150,
            
            // 记号点是否可编辑
            editable: true,
            
            // 记号点颜色
            color: "#00ff00",
            
            // 记号点标记文本
            labelText: "A point"
        },
    ],

    /**
      * debug 配置, 诊断或错误信息将写入此函数, 默认为 console.Error
      * */
    logger: console.error.bind(console)
};
```

## 4.1、标记自定义

Peaks.js 允许您自定义点和段标记的外观，通过指定以下配置选项：`createPointMarker`、`createSegmentMarker`和 `createSegmentLabel`。
请阅读 [Customizing.md](stomizing.md) 了解详情。

## 4.2、播放器定制

默认情况下，Peaks.js 通过`mediaElement`配置选项支持使用 `<audio>`或`<video>` 元素进行音频播放。通过配置 `player` 选项，Peaks.js 还允许您使用自己的定制媒体播放器库。更多详情请阅读 [自定义Peaks.js（Customizing.md#media-play）](Customizing.md#media-play)。

## 4.3、时间标签定制

Peaks.js 允许您使用`FormatPlayheadTime` 和 `FormatAxisTime`选项来自定义时间轴上及播放指针所属时间标签的外观。更多详情请阅读 [自定义Peaks.js（Customizing.md#time-Labels）](Customizing.md#time-Labels)。

# 5、API

## 5.1、生命周期相关 API

根 `Peaks` 对象公开了一个工厂函数来创建新的`Peaks`实例，即 `Peaks.init(options, callback)`

### 5.1.1、`Peaks.init(options, callback)`

**param：**

- `option` - [配置参数集对象](#4、配置)
- `callback` - 该回调在实例创建和初始化后调用，或者在初始化过程中出现错误时调用，其包含两个参数：
  - `err` - 异常对象
  - `peaks` - Peaks 实例对象

**return：**

- `Peaks` - `Peaks` 示例对象

通过 `option` 创建一个新的`Peaks`实例。`callback` 将在实例创建和初始化后调用，或者在初始化过程中出现错误时调用。您可以使用一个或多个配置在一个页面内创建和管理多个`Peaks`实例。

```js
const options = { ... };

Peaks.init(options, function(err, peaks) {
  if (err) {
    console.error('Failed to initialize Peaks instance: ' + err.message);
    return;
  }

  console.log(peaks.player.getCurrentTime());
});
```

### 5.1.2、`instance.setSource(options, callback)`

**param：**

- `option` - 配置项
  - `mediaUrl` - 音频或视频媒体URL，如果你正在使用 `<audio>` 或 `<video>` 元素，这是必需的，如果你正在使用 [自定义播放器对象](customization .md#media-playback)，则无需配置该配置项。
  - `dataUri` -【可选项】如果从服务器请求波形数据，该对象应该包含 `arraybuffer, json`的其中一项或全部
    - `arraybuffer` - 【可选项】要请求的二进制格式波形数据的 URL (.dat 格式)
    - `json` - 【可选项】请求的 JSON 格式波形数据的URL
  - `waveformData` -【可选项】 如果使用现有已解析的波形数据，该对象应该包含 `arraybuffer, json`的其中一项或全部
    - `arraybuffer` - 【可选项】二进制波形数据 (.dat 格式)
    - `json` - 【可选项】JSON 格式波形数据
  - `webAudio` - 【可选项】如果使用 Web Audio API 来生成波形，这应该是一个包含以下值的对象：
    - `audioContext` - 【可选项】一个 Web Audio `AudioContext` 实例，用于计算来自媒体的波形数据
    - `audioBuffer` - 【可选项】一个 Web Audio `AudioBuffer`实例，包含解码的音频样本。如果已存在解码的音频样本，则使用此音频数据，无需获取 `mediaUrl`
    - `multiChannel` - 【可选项】波形数据是否显示多声道，如果为 `true`，波形将显示所有可用通道。如果 `false (默认值)`，音频将显示为单个声道波形
  - `withCredentials` - 【可选项】是否携带凭证，如果`true`，当从服务器请求波形数据时，Peaks.js 将发送凭证
  - `zoomLevels` - 【可选项】样本中每像素的缩放级别数组。如果不存在，将默认使用初始化该示例时 [即 Peaks.init()](#5.1.1、`Peaks.init(options, callback)`) 所设定的值
- `callback` - 回调函数
  - `err` - 异常对象

该 API 用于更改`Peaks`实例关联的音频或视频媒体源。

为了`Peaks`实例可以更新波形视图，当您想要更改音频或视频媒体 URL 时，你应该调用此方法，而不是直接设置媒体元素的`src`属性，倘若您使用的是 [自定义播放器对象](stomizing.md#media-playback)，那么更改播放器中的音频或视频内容是您的责任，但您也应该主动调用该方法来更新波形视图。

根据指定的`options`，波形数据要么从服务器请求（服务器预先计算波形数据），要么由浏览器使用 Web Audio API 生成。

对于 `options`参数来说，其只能同时被指定为`dataUri`、`wavelformData`或`webAudio`中的一个。

例如，要更改媒体 URL 并从服务器请求新的预先计算的波形数据：

```js
const options = {
  mediaUrl: '/sample.mp3',
  dataUri: {
    arraybuffer: '/sample.dat',
    json: '/sample.json',
  }
};

instance.setSource(options, function(error) {
  // Waveform updated
});
```

或者，更改媒体 URL 并使用 Web Audio API 来生成波形:

```js
const audioContext = new AudioContext();

const options = {
  mediaUrl: '/sample.mp3',
  webAudio: {
    audioContext: audioContext,
    multiChannel: true
  }
};

instance.setSource(options, function(error) {
  // Waveform updated
});
```

### 5.1.3、`instance.destroy()`

该 API 用于释放 Peaks 实例所使用的资源。当在单页应用程序中重新初始化 Peaks.js 时，这可能很有用。

```js
instance.destroy();
```

## 5.2、Player  相关 API

### 5.2.1、`instance.player.play()`

从当前时间位置开始播放媒体。

```js
instance.player.play();
```

### 5.2.2、`instance.player.pause()`

暂停媒体播放。

```js
instance.player.pause();
```

### 5.2.3、`instance.player.getCurrentTime()`

**return：**

- `time` - `number` 类型，当前时间，单位为秒

```js
const time = instance.player.getCurrentTime();
```

### 5.2.4、`instance.player.getDuration()`

**return：**

- `time` - `number` 类型，当前媒体总时长，单位为秒

```js
const duration = instance.player.getDuration();
```

### 5.2.5、`instance.player.seek(time)`

**param：**

- `time` - `number` 类型，给定跳转时间，单位秒

查找媒体元素到给定时间，以秒为单位。

```js
instance.player.seek(5.85);
```

### 5.2.6、`instance.player.playSegment(segment[, loop])`

**param：**

- `segment` - `instance.segments` 类型，播放指定时间片段
- `isLoop` - 是否循环播放

播放给定的媒体片段，可选循环播放。

```js
const segment = instance.segments.add({
  startTime: 5.0,
  endTime: 15.0,
  editable: true
});

// 从5.0到15.0播放，然后停止
instance.player.playSegment(segment);

// 从5.0到15.0播放，然后从 5.0 重复循环
instance.player.playSegment(segment, true);
```

## 5.3、Views 相关 API

单个Peaks实例最多可以有两个相关联的波形视图：缩放视图(zoomview) 和 概略视图(overview)，Views 相关 API 允许您创建或获取对这些视图的引用。

### 5.3.1、`instance.views.getView(name)`

**param：**

- `name` - `string` 类型，当仅使用单个视图时可以省略该参数，否则需要明确指定其为`zoomview` 或 `overview` 中的一项

**return：**

- `view` - 指定的视图引用对象

```js
const view = instance.views.getView('zoomview');
```

### 5.3.2、`instance.views.createZoomview(container)`

**param：**

- `container` - `Element` 类型或继承于 `Element` 类型，一个用于承载 `zoomview` 的空容器元素

**return：**

- `view` - `zoomview` 视图引用对象

```js
const container = document.getElementById('zoomview-container');
const view = instance.views.createZoomview(container);
```

### 5.3.3、`instance.views.createOverview(container)`

**param：**

- `container` - `Element` 类型或继承于 `Element` 类型，一个用于承载 `overview` 的空容器元素

**return：**

- `view` - `overview` 视图引用对象

```js
const container = document.getElementById('overview-container');
const view = instance.views.createOverview(container);
```

### 5.3.4、`instance.views.destroyZoomview()`

删除指定 Peaks 示例对象的缩放波形视图

```js
instance.views.destroyZoomview();

const container = document.getElementById('zoomview-container');
container.style.display = 'none';
```

### 5.3.5、`instance.views.destroyOverview()`

删除指定 Peaks 示例对象的概略波形视图

```js
instance.views.destroyOverview();

const container = document.getElementById('overview-container');
container.style.display = 'none';
```

## 5.4、View 相关 API

一些视图属性可以通过 javascript 方式进行更新。

### 5.4.1、`view.setAmplitudeScale(scale)`

**param：**

- `scale` - `number` 类型，波形垂直振幅的变换尺度，默认为 1.0，如果大于1.0，则波形高度增加。如果在 0.0 和 1.0 之间，波形高度降低

```js
const view = instance.views.getView('zoomview');
view.setAmplitudeScale(1.0);
```

### 5.4.2、`view.setWaveformColor(color)`

**param：**

- `color` - `string` 类型或 `Object` 类型，指定的波形颜色，其可以为任何有效的 [CSS颜色值](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value)

初始波形颜色由 `waveformColor`，`zoomview.waveformColor`， `overview.waveformColor`配置选项控制。

```js
const view = instance.views.getView('zoomview');
view.setWaveformColor('#800080'); // Purple
```

你也可以在这里使用二阶线性颜色渐变。单位是视图高度的百分比，其应用将从波形的顶部开始。

```js
view.setWaveformColor({
  linearGradientStart: 15,
  linearGradientEnd: 30,
  linearGradientColorStops: ['hsl(120, 78%, 26%)', 'hsl(120, 78%, 10%)']
});
```

### 5.4.3、`view.setPlayedWaveformColor(color)`

**param：**

- `color` - `string` 类型或 `Object` 类型 或 `null`，指定的已播放区波形颜色，其可以为任何有效的 [CSS颜色值](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value) ，当值为 `null` 时用于清除已播放波形区域的颜色

初始已播放波形颜色由 `playedWaveformColor`，`zoomview.playedWaveformColor`， `overview.playedWaveformColor`配置选项控制。

```js
const view = instance.views.getView('zoomview');
view.setPlayedWaveformColor('#800080'); // Purple
```

你也可以在这里使用二阶线性颜色渐变。单位是视图高度的百分比，其应用将从波形的顶部开始。
```js
view.setPlayedWaveformColor({
  linearGradientStart: 15,
  linearGradientEnd: 30,
  linearGradientColorStops: ['hsl(120, 78%, 26%)', 'hsl(120, 78%, 10%)']
});
```

### 5.4.4、`view.showPlayheadTime(show)`

**param：**

- `show` - `boolean` 类型，显示或隐藏当前播放时间，其显示位置在播放指针旁边

```js
const view = instance.views.getView('zoomview');
view.showPlayheadTime(false); // 从播放头标记中删除时间
```

### 5.4.5、`view.setTimeLabelPrecision(precision)`

**param：**

- `precision` - `number` 类型，时间精度位，用于更改播放指针和点-段标记显示的时间标签的精度

```js
const view = instance.views.getView('zoomview');
view.setTimeLabelPrecision(3); // 显示播放指针与点-段的时间为 hh:mm:ss.sss
```

### 5.4.6、`view.showAxisLabels(show)`

**param：**

- `show` - `boolean` 类型，用于显示或隐藏时间轴网格刻度线时间戳标签

```js
const view = instance.views.getView('zoomview');
view.showAxisLabels(false); // 移除时间轴标签
```

### 5.4.7、`view.enableAutoScroll(enable)`

**param：**

- `enable` - `boolean` 类型，启用或禁用自动滚动行为(默认启用)，这只适用于缩放波形视图

```js
const view = instance.views.getView('zoomview');
view.enableAutoScroll(false);
```

### 5.4.8、`view.enableMarkerEditing(enable)`

**param：**

- `enable` - `boolean` 类型，启用或禁用点和段标记的编辑。默认情况下，可缩放的波形视图允许标记编辑，而概览波形视图则不允许标记编辑

注意，应该在添加任何点或段标记之前调用此方法，它不会将任何现有的不可编辑标记更改为可编辑的。

```js
const view = instance.views.getView('overview');
view.enableMarkerEditing(true);

instance.segments.add({
  startTime: 5.0,
  endTime: 10.0,
  label: 'Test segment',
  editable: true
});
```

### 5.4.9、`view.fitToContainer()`

调整波形视图的大小以适应容器。你应该在改变容器 HTML 元素的宽度或高度之后主动调用这个方法。

如果缩放级别已设置为秒数或 `auto`，波形将自动重新缩放以适应容器宽度。由于重新计算波形可能需要很长时间（特别是对于较长的波形），因此建议在更改容器宽度时使用 `debounce` 函数，例如 `lodash` 的 [_.debounce()](https://lodash.com/docs/#debounce)。

```js
const container = document.getElementById('zoomview-container');
const view = instance.views.getView('zoomview');

container.setAttribute('style', 'height: 300px');
view.fitToContainer();

// 或者设置为时间间隔为 500ms 的反射

window.addEventListener('resize', _.debounce(function() {
  view.fitToContainer();
}, 500);
```

### 5.4.10、`view.setZoom(options)`

**param：**

- `option` - `options` 为一个具有以下键之一的对象
  - `scale` - `number`类型或`string`类型，设置缩放级别，以每个像素的样本为单位，数字越小，缩放程度就越大，若值为 `auto` 则它将整个波形与容器宽度匹配
  - `seconds` - `number`类型或`string`类型，设置缩放级别以适应可用宽度中的给定秒数，若值为 `auto` 则它将整个波形与容器宽度匹配

更改可缩放波形视图的缩放级别，该方法为应用程序提供了比旧的 [Zoom 相关 API](#5.5、Zoom 相关 API) 方法更大的缩放级别控制。

```js
const view = instance.views.getView('zoomview');
view.setZoom({ scale: 512 }); // samples per pixel
view.setZoom({ seconds: 5.0 });
view.setZoom({ seconds: 'auto' });
```

### 5.4.11、`view.setStartTime(time)`

**param：**

- `time` - `number` 类型，用于更改可缩放波形视图的开始时间，以秒为单位。

注意，该方法在概览波形上不可用。

```js
const view = instance.views.getView('zoomview');
view.setStartTime(6.0); // seconds
```

### 5.4.12、`view.scrollWaveform(options)`

**param：**

- `option` - `options` 为一个具有以下键之一的对象
  - `seconds` - `number`类型或`string`类型，按给定的秒数滚动缩放波形视图，传递一个负数将波形向左滚动
  - `pixels` - `number`类型或`string`类型，按给定的像素数滚动缩放波形视图，传递一个负数将波形向左滚动

注意，该方法在概览波形上不可用。

```js
const view = instance.views.getView('zoomview');
view.scrollWaveform({ seconds: 1.0 });
view.scrollWaveform({ pixels: -100 });
```

### 5.4.13、`view.setWheelMode(mode[, options])`

**param：**

- `mode` - `string` 类型，其可选值为：
  -  `none` - 禁用鼠标滚轮输入，该值为默认值
  - `scroll` - 滚动波形视图
- `option` - `option` 参数允许自定义行为，其应该是一个包含以下键之一的对象
  - `captureVerticalScroll` - `boolean` 类型，控制当鼠标定位在波形上时，鼠标滚轮是滚动波形，还是滚动页面，默认为 `false`（即滚动页面）

控制波形视图如何响应鼠标滚轮输入。在笔记本电脑的触控板上，这通常是一个水平滑动的手势。对于鼠标带有滚轮的用户，在使用滚轮时需要按住Shift键。

注意，这种方法在概览波形上不可用。

```js
const view = instance.views.getView('zoomview');
view.setWheelMode('scroll');
view.setWheelMode('scroll', { captureVerticalScroll: true });
```

### 5.4.14、`view.enableSeek(enable)`

**param：**

- `enable` - `boolean` 类型，其表示在波形视图中单击，启用或禁用查找回放位置

```js
const overview = peaksInstance.views.getView('zoomview');
const zoomview = peaksInstance.views.getView('zoomview');

overview.enableSeek(false); // 为 true 时重新启用
zoomview.enableSeek(false);
```

## 5.5、Zoom 相关 API

### 5.5.1、`instance.zoom.zoomOut()`

将波形缩略视图放大一级，直至最大级。（译者注：每一级的比例基于 `zoomLevels` 配置）

```js
Peaks.init({
  // ...
  zoomLevels: [512, 1024, 2048, 4096]
},
function(err, peaks) {
  // 初始缩放级别为 512
  peaks.zoom.zoomOut(); // 缩放等级现在是 1024
});
```

### 5.5.2、`instance.zoom.zoomIn()`

将波形缩略视图缩小一级，直至最小级。（译者注：每一级的比例基于 `zoomLevels` 配置）

```js
Peaks.init({
  // ...
  zoomLevels: [512, 1024, 2048, 4096]
},
function(err, peaks) {
  // 初始缩放级别为 512
  peaks.zoom.zoomIn(); // 缩放级别仍为 512

  peaks.zoom.zoomOut(); // 缩放等级现在是 1024
  peaks.zoom.zoomIn(); // 缩放等级现在重新是 512
});
```

### 5.5.3、`instance.zoom.setZoom(index)`

**param：**

- `index` - `number` 类型，指定缩放级索引

将缩放波形视图的缩放级别更改为给定 `index` 索引项的级数。

```js
Peaks.init({
  // ...
  zoomLevels: [512, 1024, 2048, 4096]
},
function(err, peaks) {
  peaks.zoom.setZoom(3); // zoom level is now 4096
});
```

除此之外还可以参阅 [view.setZoom()](#5.4.10、`view.setZoom(options)`)，它提供了一种更灵活的设置缩放级别的方法。

### 5.5.4、`instance.zoom.getZoom()`

**return：**

- `index` - `number` 类型，当前缩放级别索引

```js
Peaks.init({
  // ...
  zoomLevels: [512, 1024, 2048, 4096]
},
function(err, peaks) {
  peaks.zoom.zoomOut();
  console.log(peaks.zoom.getZoom()); // -> 1
});
```

## 5.6、Segments  相关 API

**记号段** 提供视觉标记音频媒体的定时部分的能力。这是一种向用户提供视觉提示的好方法。

### 5.6.1、`instance.segments.add({ startTime, endTime, editable, color, labelText, id[, ...] }) 或 instance.segments.add(segment[])`
**param：**

- `option` - 其包含以下参数
  - `startTime` - `number` 类型，记号段开始时间，单位秒
  - `endTime` - `number` 类型，记号段结束时间，单位秒
  - `editable` - 【可选项】`boolean` 类型，设置段是否用户可编辑，默认为 `false`
  - `color` - 【可选项】`string` 类型 或 `Object` 类型，分段颜色。如果没有指定，段将被赋予一个默认颜色(参见 `segmentColor` 和
    `randomizeSegmentColor` [配置](#4、配置))
  - `labelText` - 【可选项】`string` 类型，当用户将鼠标指针悬停在记号段上时显示的文本
  - `id` - 【可选项】`string` 类型，记号段标识符。如果未指定，则自动给段一个唯一标识符

该 API 用于将一个记号段添加到波形时间轴。

```js
// 添加不可编辑的片段，从 0 到 10.5 秒，随机颜色
instance.segments.add({ startTime: 0, endTime: 10.5 });
```

或者，提供一个段对象数组来一次添加所有这些段。这样做比一次添加一个片段要有效得多。

```js
instance.segments.add([
  {
    startTime: 0,
    endTime: 10.5,
    labelText: '0 to 10.5 seconds non-editable demo segment'
  },
  {
    startTime: 3.14,
    endTime: 4.2,
    color: '#666'
  }
]);
```

您还可以提供其他用户定义的数据属性，这些属性与段相关联。这些可以是字符串、数字或任何其他 JavaScript 对象。

```js
instance.segments.add({ id: 'segment1', startTime: 0, endTime: 10.5, customAttribute: 'value' });

const segment = instance.segments.getSegment('segment1');

console.log(segment.customAttribute); // -> 'value'
```

### 5.6.2、`instance.segments.getSegments()`

**return：**

- `segmentArr` - `segment` 实例数组

返回时间轴上所有记号段实例的数组。

```js
const segments = instance.segments.getSegments();
```

### 5.6.3、`instance.segments.getSegment(id)`

**param：**

- `id` - `segment` 实例 `id`

**return：**

- `segment` - `segment` 实例或 `null`

返回具有给定 `id` 的记号段，如果未找到则返回 `null`。

```js
const segment = instance.segments.getSegment('peaks.segment.3');
```

### 5.6.4、`instance.segments.removeByTime(startTime[, endTime])`

**param：**

- `startTime` - `number` 类型，起始时间，单位秒
- `endTime` - 【可选项】`number` 类型，结束时间，单位秒

**return：**

- `delSegmentNum` - `number` 类型，删除的 `segment` 实例数量

删除从 `startTime` 秒开始，并可选地结束于`endTime` 秒的所有记号段。

```js
instance.segments.add([
  { startTime: 10, endTime: 12 },
  { startTime: 10, endTime: 20 }
]);

// Remove both segments as they start at `10`
instance.segments.removeByTime(10);

// Remove only the first segment
instance.segments.removeByTime(10, 12);
```

### 5.6.5、`instance.segments.removeById(segmentId)`

**param：**

- `segmentId` - `string` 类型，`segment` 唯一性索引

删除具有给定标识符的记号段。

```js
instance.segments.removeById('peaks.segment.3');
```

### 5.6.6、`instance.segments.removeAll()`

删除所有记号段。

```js
instance.segments.removeAll();
```

## 5.7、Segment 相关 API

### 5.7.1、`segment.update({ startTime, endTime, labelText, color, editable[, ...] })`

**param：**

- `option` - 包含以下字段的配置对象
  - `startTime` - 【可选项】`number` 类型，记号段开始时间，单位秒，默认为当前值
  - `endTime` - 【可选项】`number` 类型，记号段结束时间，单位秒，默认为当前值
  - `editable` - 【可选项】`boolean` 类型，设置当前记号段是否用户可编辑，默认为当前值
  - `color` - 【可选项】`string` 类型或 `Object`类型，记号段颜色，默认为当前值
  - `labelText` - 【可选项】`string` 类型，当用户将鼠标指针悬停在记号段上时显示的文本标签，默认为当前值

更新一个现有的记号段。接受单个 `options` 参数，您还可以利用该 API 更新其他用户自定义的与记号段相关联的数据属性。

```js
instance.segments.add({ ... });

const segment = instance.segments.getSegments()[0]
// 或者使用 peaks.segments.getSegment(id)

segment.update({ startTime: 7 });
segment.update({ startTime: 7, labelText: "new label text" });
segment.update({ startTime: 7, endTime: 9, labelText: 'new label text' });

// 更新用户的自定义属性 customAttribute
segment.update({ customAttribute: 'value' });
```

## 5.8、Points 相关 API

**记号点** 提供视觉标记音频媒体时间点的能力。

### 5.8.1、`instance.points.add({ time, editable, color, labelText, id[, ...] }) 或 instance.points.add(point[])`
**param：**

- `option` - 包含以下字段的配置对象
  - `time` - `number` 类型，记号点时间，单位秒
  - `editable` - 【可选项】`boolean` 类型，设置当前记号点是否用户可编辑，默认为 `false`
  - `color` - 【可选项】`string` 类型或 `Object`类型，记号点颜色，默认为当前值
  - `labelText` - 【可选项】`string` 类型，当用户将鼠标指针悬停在记号点上时显示的文本标签，默认为当前值
  - `id` - 【可选项】`string` 类型，记号点标识符。如果未指定，则自动给段一个唯一标识符

```js
// 添加不可编辑的记号点，用随机的颜色
instance.points.add({ time: 3.5 });
```

或者直接提供一个点对象数组，这样做比一次添加一个点要高效得多。

```js
instance.points.add([
  {
    time: 3.5,
    labelText: 'Test point',
    color: '#666'
  },
  {
    time: 5.6,
    labelText: 'Another test point',
    color: '#666'
  }
]);
```

您还可以提供其他用户定义的数据属性，这些属性与点相关联。这些可以是字符串、数字或任何其他 JavaScript 对象。

```js
instance.points.add({ id: 'point1', time: 3.5, customAttribute: 'value' });

const point = instance.points.getSegment('point1');

console.log(point.customAttribute); // -> 'value'
```

### 5.8.2、`instance.points.getPoints()`

**return：**

- `pointArr` - `point` 实例数组

返回时间轴上所有记号点的数组。

```js
const points = instance.points.getPoints();
```

### 5.8.3、`instance.points.getPoint(id)`

**param：**

- `id` - `string` 类型，`point` 实例唯一性索引

**return：**

- `point` - `point` 实例或 `null`

返回具有给定 `id` 的记号点，如果未找到则返回 `null`

```js
const point = instance.points.getPoint('peaks.point.3');
```

### 5.8.4、`instance.points.removeByTime(time)`

**param：**

- `time` - `number`类型，给定时间，单位秒

删除给定的 `time` 秒的任何记号点。

```js
instance.points.removeByTime(10);
```

### 5.8.5、`instance.points.removeById(pointId)`

**param：**

- `pointId` - `string` 类型，`point` 实例唯一性索引

删除具有给定标识符的记号点。

```js
instance.points.removeById('peaks.point.3');
```

### 5.8.6、`instance.points.removeAll()`

删除所有记号点。

```js
instance.points.removeAll();
```

## 5.9、Point 相关 API

### 5.9.1、`point.update({ time, labelText, color, editable[, ...] })`

**param：**

- `option` - 包含以下字段的配置对象
  - `time` - 【可选项】`number` 类型，记号点开始时间，单位秒，默认为当前值
  - `editable` - 【可选项】`boolean` 类型，设置当前记号点是否用户可编辑，默认为当前值
  - `color` - 【可选项】`string` 类型或 `Object`类型，记号点颜色，默认为当前值
  - `labelText` - 【可选项】`string` 类型，当用户将鼠标指针悬停在记号点上时显示的文本标签，默认为当前值

更新一个现有的记号点。接受单个 `options` 参数，您还可以利用该 API 更新其他用户自定义的与记号点相关联的数据属性。

```js
instance.points.add({ ... });
const point = instance.points.getPoints()[0]
// 或者使用 instance.points.getPoint(id)

point.update({ time: 7 });
point.update({ time: 7, labelText: "new label text" });

// 更新用户自定义属性 customAttribute
point.update({ customAttribute: 'value' });
```

# 6、事件

`Peaks` 实例会抛出一系列事件，您能够根据需要扩展其相关事件监听行为。

## 6.1、`instance.on(event, callback)`

**param：**

- `event` - `string` 类型，事件名称
- `callback` - 事件监听器
  - `event` - 继承于 `Event` 类型，事件对象

注册一个回调函数来处理由 `Peaks` 实例发出的事件。

```js
function dblClickHandler(event) {
  console.log(event.time); // 用户单击的时间位置
  console.log(event.evt.ctrlKey); // 访问 MouseEvent 属性
}

instance.on('zoomview.dblclick', dblClickHandler);
```

## 6.2、`instance.once(event, callback)`

**param：**

- `event` - `string` 类型，事件名称
- `callback` - 事件监听器
  - `event` - 继承于 `Event` 类型，事件对象

注册一个回调函数来处理由 Peaks 实例发出的一次性事件

```js
function dblClickHandler(event) {
  console.log(event.time); // 用户单击的时间位置
  console.log(event.evt.ctrlKey); // 访问M ouseEvent 属性
}

instance.once('zoomview.dblclick', dblClickHandler);
```

## 6.3、`instance.off(event, callback)`

**param：**

- `event` - `string` 类型，事件名称
- `callback` - 事件监听器函数

删除 Peaks 实例上给定的事件监听器函数。

（译者注：若事件监听器函数注册时为匿名函数，则无法删除）

```js
instance.off('zoomview.dblclick', dblClickHandler);
```

## 6.4、相关事件

### 6.4.2、Player 相关事件6.4.1、生命周期相关事件

| 事件名        | 参数   |
| ------------- | ------ |
| `peaks.ready` | (none) |

### 6.4.2、Player 相关事件

| 事件名              | 参数          |
| ------------------- | ------------- |
| `player.canplay`    | (none)        |
| `player.error`      | `Error error` |
| `player.pause`      | `Number time` |
| `player.playing`    | `Number time` |
| `player.seeked`     | `Number time` |
| `player.timeupdate` | `Number time` |
| `player.ended`      | (none)        |

### 6.4.3、Views 相关事件

| 事件名              | 参数                           |
| ------------------- | ------------------------------ |
| `overview.click`    | `WaveformViewClickEvent event` |
| `overview.dblclick` | `WaveformViewClickEvent event` |
| `zoomview.click`    | `WaveformViewClickEvent event` |
| `zoomview.dblclick` | `WaveformViewClickEvent event` |

### 6.4.4、Waveforms 相关事件

| 事件名        | 参数                                                  |
| ------------- | ----------------------------------------------------- |
| `zoom.update` | `Number currentZoomLevel`, `Number previousZoomLevel` |

### 6.4.5、Segments 相关事件

| 事件名                | 参数                      |
| --------------------- | ------------------------- |
| `segments.add`        | `Array<Segment> segments` |
| `segments.remove`     | `Array<Segment> segments` |
| `segments.remove_all` | (none)                    |
| `segments.dragstart`  | `SegmentDragEvent event`  |
| `segments.dragged`    | `SegmentDragEvent event`  |
| `segments.dragend`    | `SegmentDragEvent event`  |
| `segments.mouseenter` | `SegmentEvent event`      |
| `segments.mouseleave` | `SegmentEvent event`      |
| `segments.click`      | `SegmentEvent event`      |
| `segments.dblclick`   | `SegmentEvent event`      |

### 6.4.6、Points 相关事件

| 事件名              | 参数                  |
| ------------------- | --------------------- |
| `points.add`        | `Array<Point> points` |
| `points.remove`     | `Array<Point> points` |
| `points.remove_all` | (none)                |
| `points.dragstart`  | `PointEvent event`    |
| `points.dragmove`   | `PointEvent event`    |
| `points.dragend`    | `PointEvent event`    |
| `points.mouseenter` | `PointEvent event`    |
| `points.mouseleave` | `PointEvent event`    |
| `points.click`      | `PointEvent event`    |
| `points.dblclick`   | `PointEvent event`    |

### 6.4.7、提示事件

要启用提示事件，请使用 `{ emitCueEvents: true }` 选项初始化相关参数 `Peaks.init()`，当播放头到达一个记号点或记号段边界时，将触发一个提示事件。

| 事件名           | 参数              |
| ---------------- | ----------------- |
| `points.enter`   | `Point point`     |
| `segments.enter` | `Segment segment` |
| `segments.exit`  | `Segment segment` |

# 7、构建 Peaks.js

本节描述如何在本地构建Peaks.js，如果你想修改代码或做出更改。

## 7.1、前置操作

```bash
git clone git@github.com:bbc/peaks.js.git
cd peaks.js
npm install
```

## 7.2、构建

该命令将生成兼容 UMD 的独立版本的 Peaks.js，包括简化版和非简化版。你可以使用 AMD 或 CommonJS 模块加载器，甚至作为普通的 JavaScript 使用。

```bash
npm run build
```

构建的输出是名为 `peaks.js` 和 `peaks.min.js` 的文件，以及它们相关的 [source maos](https://hacks.mozilla.org/2013/05/compiling-to-javascript-and-debugging-with-source-maps/)。

## 7.3、测试

在 Karma 中使用 Mocha + Chai + Sinon 进行测试。

 - `npm test` 应该适用于简单的一次性测试。
 - `npm test -- --glob %pattern%`  只运行选定的测试套件
 - `npm run test-watch` 如果您正在进行开发，并且希望在您机器上的浏览器中重复运行测试
 - `npm run test-watch -- --glob %pattern%` 如果您正在进行开发，并且希望在您机器上的浏览器中重复运行选定的测试套件

# 贡献

如果你想为Peaks.js做贡献，请看看我们的 [贡献者指南](https://github.com/bbc/peaks.js/blob/master/CONTRIBUTING.md)

# License 开源许可协议

详细可见 [COPYING](https://github.com/bbc/peaks.js/blob/master/COPYING)，该项目包括来自BBC广播节目 [荒岛唱片](https://en.wikipedia.org/wiki/File:Alice_walker_bbc_radio4_desert_island_discs_19_05_2013.flac) 的音频样本，根据 [创作共用3.0未移植许可协议](http://creativecommons.org/licenses/by/3.0/) 的条款使用。

# 开发人员

本软件由以下人员编写：

- [Chris Finch](https://github.com/chrisfinch)
- [Thomas Parisot](https://github.com/oncletom)
- [Chris Needham](https://github.com/chrisn)

也感谢所有的 [贡献者](https://github.com/bbc/peaks.js/graphs/contributors)

# 版权申明

版权所有：英国广播公司
