# ReferenceDocument

# 1、现收录文档

## 1.1、peaks.js

|      文件      |                    是否翻译                    |      备注      |
| :------------: |:------------------------------------------:| :------------: |
|   README.md    | <span style="color: greenyellow;">是</span> |     主文档     |
| customizing.md |     <span style="color: red;">是</span>     | 用户自定义文档 |

